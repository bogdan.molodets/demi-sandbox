var soundentity = null;
var isTargetFound = false;
var interacted = false;
var dino = null;

AFRAME.registerComponent("markerhandler", {
    init: function () {
        console.log('markerhandler-init');

        markerObj = document.querySelector('a-marker');
        markerObj.hidden = true;
        soundentity = document.querySelector('[sound]');
        dino = document.querySelector('#warm');
        dino.object3D.visible = false;
    },

    tick: function () {

        if (markerObj != null) {
            if (markerObj.object3D.visible == true) {
                if (isTargetFound)
                    return;

                isTargetFound = true;
                dino.object3D.visible = true;
                soundentity.components.sound.playSound();

            } else {
                if (!isTargetFound)
                    return;

                isTargetFound = false;
                dino.object3D.visible = false;
                soundentity.components.sound.stopSound();

            }
        }
    }
});