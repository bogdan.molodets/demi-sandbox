const express = require('express');
const app = express();
const path = require('path');
const public = path.join(__dirname, 'public');

app.use('/', express.static(public));


app.get('/', function(req, res) {
    res.sendFile(path.join(public, 'index.html'));
});

const port = process.env.PORT || 3000;


app.listen(port, function() {
  console.log("Server started.......");
  console.log(port);
});
